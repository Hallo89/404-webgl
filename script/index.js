'use strict';
import { Controller } from './Controller.js';
import * as webgl from './gl.js';

window.controller = new Controller();
window.webgl = webgl;
