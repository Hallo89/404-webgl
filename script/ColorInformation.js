import { DIMENSIONS } from './GeometryParser.js';

const HUE_MOUSEMOVE_THRESHOLD = 90;

const DEFAULT_COLOR = {
  front: [ .93, .93, .93 ],
  back: [ .35, .35, .35 ],
  side: [ .59, .59, .59 ]
};

export class ColorInformation {
  #snapshots;
  #stencil;
  data;

  /**
   * @param { Uint8Array<ArrayBuffer> } stencil Undivided color stencil buffer.
   * @param { number } size Vertex size.
   */
  constructor(points, stencil, size) {
    this.data = new Float32Array(stencil.length);
    this.#stencil = stencil;
    this.#snapshots = {
      distance: new Float32Array(stencil.length),
      default: new Float32Array(stencil.length),
    };

    this.#computeDefaultColors(points);
    this.setDefault();
  }

  /**
   * Paint the model's vertices with a decreasing hue based on
   * how far the point is away from the supplied `localDistance`.
   * This creates a circular gradient with `localDistance` as its center.
   */
  setByDistance(points, index, localDistance, startingHue) {
    for (var i = 0; i < points.length; i += 3) {
      const pointX = points[i]     / DIMENSIONS.x;
      const pointY = points[i + 1] / DIMENSIONS.y;
      const pointZ = points[i + 2] / DIMENSIONS.z;

      const BACK_LIGHTNESS = 26;

      let lightness;
      let saturation;
      // Determine whether the current vertex is front, back or side
      switch (this.#stencil[i]) {
        case 0:
          lightness = 71;
          saturation = 52;
          break;
        case 1:
          lightness = BACK_LIGHTNESS;
          saturation = 25;
          break;
        case 2:
          lightness = pointZ ? 49 : BACK_LIGHTNESS;
          saturation = 35;
          break;
      }
      const distance = Math.sqrt(
          Math.pow(localDistance.x[index] - pointX, 2)
        + Math.pow(localDistance.y - pointY, 2));

      const usedHue = (distance * HUE_MOUSEMOVE_THRESHOLD + startingHue + 360) % 360;
      const color = ColorInformation.hslToRgb(usedHue, saturation, lightness);

      this.data[i]     = color[0];
      this.data[i + 1] = color[1];
      this.data[i + 2] = color[2];

      this.#snapshots.distance[i]     = color[0];
      this.#snapshots.distance[i + 1] = color[1];
      this.#snapshots.distance[i + 2] = color[2];
    }
  }

  /** Copy the precomputed default colors into the color data */
  setDefault() {
    for (let i = 0; i < this.#snapshots.default.length; i++) {
      this.data[i] = this.#snapshots.default[i];
    }
  }

  /** Compute the default colors and save them in the snapshot for later. */
  #computeDefaultColors(points) {
    for (let i = 0; i < points.length; i += 3) {
      const pointZ = points[i + 2] / DIMENSIONS.z;
      let color;
      switch (this.#stencil[i]) {
        case 0:
          color = DEFAULT_COLOR.front;
          break;
        case 1:
          color = DEFAULT_COLOR.back;
          break;
        case 2:
          color = pointZ ? DEFAULT_COLOR.side : DEFAULT_COLOR.back;
          break;
      }

      this.#snapshots.default[i]     = color[0];
      this.#snapshots.default[i + 1] = color[1];
      this.#snapshots.default[i + 2] = color[2];
    }
  }

  /**
   * Interpolate the saved snapshot color into the default color
   * at the given progress (interval [0, 1]) directly into the
   * current color data.
   *
   * @param { number } progress
   */
  interpolateFromSnapshotToDefault(progress) {
    for (let i = 0; i < this.#snapshots.default.length; i++) {
      const defaultColor = this.#snapshots.default[i];
      const distanceColor = this.#snapshots.distance[i];
      // console.log(defaultColor, distanceColor, progress);
      // console.log(((defaultColor - distanceColor) * progress) + distanceColor);
      this.data[i] = ((defaultColor - distanceColor) * progress) + distanceColor;
    }
  }

  // /** Set the model's color from an RGB color. */
  // setFromRGB(front, back, side) {
  //   for (var i = 0; i < this.colors.length; i += 3) {
  //     let color;
  //     switch (this.#stencil[i]) {
  //       case 0:
  //         color = front;
  //         break;
  //       case 1:
  //         color = back;
  //         break;
  //       case 2:
  //         color = side;
  //         break;
  //     }
  //     this.colors[i] = color[0];
  //     this.colors[i + 1] = color[1];
  //     this.colors[i + 2] = color[2];
  //   }
  // }
  // /** Set the model's color from a HSL color. */
  // setFromHSL(...colors) {
  //   this.setFromRGB(
  //     Geometry.hslToRgb(...colors[0]),
  //     Geometry.hslToRgb(...colors[1]),
  //     Geometry.hslToRgb(...colors[2]));
  // }


  // --- Static helper function ---
  /**
   * Courtesy of https://gist.github.com/mjackson/5311256 with
   * some adaptations. Original comment below.
   *
   * Converts an HSL color value to RGB. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
   * Assumes h, s, and l are contained in the sets
   * [0, 360], [0, 100], [0, 100] respectively and
   * returns r, g, and b in the set [0, 1].
   *
   * @param Array hsl The [hue, saturation, lightness]
   * @return Array The RGB representation
   */
  static hslToRgb(h, s, l) {
    h /= 360;
    s /= 100;
    l /= 100;

    let r, g, b;

    if (s == 0) {
      r = g = b = l; // achromatic
    } else {
      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;

      r = hue2rgb(p, q, h + 1/3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1/3);
    }

    return [ r, g, b ];

    function hue2rgb(p, q, t) {
      if (t < 0) t += 1;
      if (t > 1) t -= 1;
      if (t < 1/6) return p + (q - p) * 6 * t;
      if (t < 1/2) return q;
      if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }
  }
}
