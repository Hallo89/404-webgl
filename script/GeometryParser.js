'use strict';
export const DIMENSIONS = {
  x: 7,
  y: 12,
  z: 3
};

export class GeometryParser {
  static parseGeometry(objFormat) {
    const stencilPattern = {
      front: [0, 0, 0],
      back: [1, 1, 1],
      side: [2, 2, 2]
    };
    /** @type { number[][] } */
    const colorStencil = [];
    /** @type { number[] } */
    const points = [];

    for (const part of objFormat) {
      const vertCount = part.vertices ? part.vertices.length : 0;

      // Front & back
      for (let i = 0; i <= vertCount - 3; i++) {
        const z = i % 2 ? 0 : 3;
        points.push(
          part.vertices[i    ], z,
          part.vertices[i + 1], z,
          part.vertices[i + 2], z,
          part.vertices[i + 2], 3 - z,
          part.vertices[i + 1], 3 - z,
          part.vertices[i    ], 3 - z
        );
        if (z == 0) {
          colorStencil.push(
            stencilPattern.back,
            stencilPattern.back,
            stencilPattern.back,
            stencilPattern.front,
            stencilPattern.front,
            stencilPattern.front
          );
        } else {
          colorStencil.push(
            stencilPattern.front,
            stencilPattern.front,
            stencilPattern.front,
            stencilPattern.back,
            stencilPattern.back,
            stencilPattern.back
          );
        }
      }
      // Sides
      for (let i = 0; i <= vertCount - 2; i++) {
        let mod = 2;
        let z = 0;

        if (i == 0) {
          points.push(
            part.vertices[i + mod], z,
            part.vertices[i], z,
            part.vertices[i], 3,
            part.vertices[i], 3 - z,
            part.vertices[i + mod], 3 - z,
            part.vertices[i + mod], z,
          );
          mod = 1;
          points.push(
            part.vertices[i], z,
            part.vertices[i + mod], z,
            part.vertices[i], 3 - z,
            part.vertices[i], 3 - z,
            part.vertices[i + mod], z,
            part.vertices[i + mod], 3 - z,
          );
          colorStencil.push(
            stencilPattern.side,
            stencilPattern.side,
            stencilPattern.side,
            stencilPattern.side,
            stencilPattern.side,
            stencilPattern.side
          );
        } else if (i == vertCount - 2) {
          mod = 1;
          points.push(
            part.vertices[i + mod], z,
            part.vertices[i], z,
            part.vertices[i], 3 - z,
            part.vertices[i], 3 - z,
            part.vertices[i + mod], 3 - z,
            part.vertices[i + mod], z,
          );
        } else {
          if (i == 0 || i == vertCount - 2) mod = 1;
          if (i != vertCount - 2 && i % 2 == 0) z = 3;

          points.push(
            part.vertices[i], z,
            part.vertices[i + mod], z,
            part.vertices[i], 3 - z,
            part.vertices[i], 3 - z,
            part.vertices[i + mod], z,
            part.vertices[i + mod], 3 - z
          );
        }
        colorStencil.push(
          stencilPattern.side,
          stencilPattern.side,
          stencilPattern.side,
          stencilPattern.side,
          stencilPattern.side,
          stencilPattern.side
        );
      }
    }

    return {
      colorStencil: new Uint8Array(colorStencil.flat()),
      points: new Float32Array(points.flat()),
    };
  }
}
