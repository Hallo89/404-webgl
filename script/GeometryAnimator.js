'use strict';
import { State3D } from '@lib/controls3d/script/State3D.js';
import { Movement } from './Movement.js';
import { State3DHandler } from './State3DHandler.js';

const ANIM_DURATION = {
  LETTER_SHIFT: 240,
  LETTER_BOUNCE: 625,
  CLICK_IN: 80,
  CLICK_OUT: 200
};

export const MOD_BASE = {
  tran: {
    x: -0.45,
    y: .72,
  }
}
const MOD_MOVE = {
  lr: {
    tran: {
      z: .55
    },
    rot: {
      x: 4,
      y: .75
    }
  },
  c: {
    tran: {
      z: 1.7
    },
    rot: {
      x: 4,
      y: 3
    }
  }
};
const MOD_CLICK = {
  lr: {
    tran: {
      z: 1.5
    },
    rot: {
      x: 6.75,
      y: 2.5
    }
  },
  c: {
    tran: {
      z: 1.5
    },
    rot: {
      x: 7.75,
      y: 5
    }
  }
};

export class GeometryAnimator {
  index;
  stateHandler;

  constructor(index) {
    this.index = index;
    this.stateHandler = new State3DHandler();

    // The rest is added dynamically in index.js
    this.stateHandler.addState('base')
      .assignNewTransform({ tran: { z: -22 } });

    this.stateHandler.addState('mousemove');
    this.stateHandler.addState('mouseclick');
    this.stateHandler.addState('animLetter');
    this.stateHandler.addState('animBounce');
  }


  // ---- State assignment ----
  setFlatMouseMove(absDistance) {
    const newTransform = Movement.getFlatMouseModelMovement(this.index, absDistance);
    this.stateHandler.states.mousemove.assignNewTransform(newTransform);
  }
  setDynamicMouseMove(absDistance) {
    const newTransform = Movement.getDynamicMouseModelMovement(this.index, absDistance, MOD_MOVE);
    this.stateHandler.states.mousemove.assignNewTransform(newTransform);
  }

  setMouseClick(absDistance) {
    const newTransform = Movement.getDynamicMouseModelMovement(this.index, absDistance, MOD_CLICK);
    this.stateHandler.states.mouseclick.assignNewTransform(newTransform);
  }

  // ---- Animations ----
  animateClickIn(absDistance, drawFn = () => {}) {
    const newTransform = Movement.getDynamicMouseModelMovement(this.index, absDistance, MOD_CLICK);
    return this.stateHandler.states.mouseclick.animateTo(ANIM_DURATION.CLICK_IN, newTransform, {
      callback: drawFn,
      easing: State3D.Easing.EASE,
    });
  }
  animateClickOut(drawFn = () => {}) {
    return this.stateHandler.states.mouseclick.animate(ANIM_DURATION.CLICK_OUT, {
      callback: drawFn,
      easing: State3D.Easing.EASE,
      steps: {
        100: {
          tran: { z: 0 },
          rot:  { x: 0, y: 0 }
        }
      }
    });
  }

  animateLetterOut(drawFn = () => {}) {
    return this.stateHandler.states.animLetter.animate(ANIM_DURATION.LETTER_SHIFT, {
      callback: drawFn,
      easing: State3D.Easing.EASE_IN_SINE,
      steps: {
        100: {
          scale: { z: -1 },
          tran:  { z: -2.5 }
        }
      },
    });
  }
  animateLetterIn(drawFn = () => {}) {
    return this.stateHandler.states.animLetter.animate(ANIM_DURATION.LETTER_SHIFT, {
      callback: drawFn,
      easing: State3D.Easing.EASE_OUT_QUAD,
      steps: {
        0: {
          scale: { z: -1 },
          tran:  { z: 2.5 }
        },
        100: {
          scale: { z: 0 },
          tran:  { z: 0 }
        }
      },
    });
  }

  animateLetterBounce(drawFn = () => {}) {
    return this.stateHandler.states.animBounce.animate(ANIM_DURATION.LETTER_BOUNCE, {
      easing: State3D.Easing.EASE_IN_OUT_SINE,
      callback: drawFn,
      steps: {
        25: {
          scale: {
            x: -.069,
            y: -.069,
            z: -.069,
          }
        },
        55: {
          scale: {
            x: .0275,
            y: .0275,
            z: .0275,
          }
        },
        78: {
          scale: {
            x: -.0075,
            y: -.0075,
            z: -.0075,
          }
        },
      }
    });
  }

  // ---- Static utilities ----
  static sleep(time) {
    return new Promise(res => {
      setTimeout(res, time);
    });
  }
}
