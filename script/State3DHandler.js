'use strict';
import { State3D } from '@lib/controls3d/script/State3D.js';

class State3DHandlerError extends Error {
  constructor(...args) {
    super(...args);
    this.name = 'State3DHandlerError';
  }
}

export class State3DHandler {
  drawFunction;

  /** @type { Record<string, State3D> } */
  states = {};
  state = {
    scale: {},
    tran: {},
    rot: {}
  };

  constructor(drawFunction) {
    this.drawFunction = drawFunction;

    // Registering getters/setters on state
    for (const type in this.state) {
      for (const axis of ['x', 'y', 'z']) {
        Object.defineProperty(this.state[type], axis, {
          get: () => {
            let value = 0;
            for (const state3D of Object.values(this.states)) {
              value += state3D.transform[type][axis];
            }
            return value;
          },
          set(val) {
            throw new State3DHandlerError('You may not assign to `state`!');
          },
          enumerable: true,
        });
      }
    }
  }


  // --- State methods ---
  /** @return { State3D } */
  addState(state3DName, initialState) {
    if (state3DName in this.states) {
      throw new State3DHandlerError(`${state3DName} is already a registered state object!`);
    }
    initialState = Object.assign({
      scale: { x: 0, y: 0, z: 0 }
    }, initialState);

    this.states[state3DName] = new State3D(initialState);
    return this.states[state3DName];
  }

  removeState(state3DName) {
    delete this.states[state3DName];
    return this.states[state3DName]
  }

  hasState(state3DName) {
    return Object.prototype.hasOwnProperty.call(this.states, state3DName);
  }

  mergeStates(targetState3DName, ...sourceState3DNames) {
    if (!this.hasState(targetState3DName)) {
      throw new State3DHandlerError(`Target state '${targetState3DName}' is not a registered state!`);
    }
    const targetState3D = this.states[targetState3DName];

    for (const sourceState3DName of sourceState3DNames) {
      if (!this.hasState(sourceState3DName)) {
        throw new State3DHandlerError(`Source state '${sourceState3DName}' is not a registered state!`);
      }
      const sourceState3D = this.states[sourceState3DName];

      for (const type in sourceState3D.transform) {
        for (const axis in sourceState3D.transform[type]) {
          targetState3D.transform[type][axis] += sourceState3D.transform[type][axis];
          sourceState3D.transform[type][axis] = 0;
        }
      }
    }
  }

  importAllStatesFrom(sourceStateHandler) {
    // NOTE: This will properly CLONE the source states, not reference them!
    for (const [ state3DName, state3D ] of Object.entries(sourceStateHandler.states)) {
      if (!this.hasState(state3DName)) {
        this.addState(state3DName);
      }
      for (const type in state3D.transform) {
        Object.assign(this.states[state3DName].transform[type], state3D.transform[type]);
      }
    }
  }

  resetState(state3DName) {
    if (!this.hasState(state3DName)) {
      throw new State3DHandlerError(`State ${state3DName} is not a registered state!`);
    }
    this.states[state3DName].assignNewTransform({
      scale: {
        x: 0,
        y: 0,
        z: 0
      },
      tran: {
        x: 0,
        y: 0,
        z: 0
      },
      rot: {
        x: 0,
        y: 0,
        z: 0
      },
    });
  }
}
