'use strict';
import { MOD_BASE } from './GeometryAnimator.js';

export class Movement {
  static getFlatMouseModelMovement(modelIndex, absDistance) {
    switch (modelIndex) {
      case 0:
        return {
          tran: {
            x: MOD_BASE.tran.x * absDistance.x.c,
            y: MOD_BASE.tran.y * this.logisticsCurveX(absDistance.x.c, -absDistance.y),
          }
        }
        break;
      case 1:
        return {
          tran: {
            x: MOD_BASE.tran.x * absDistance.x.c,
            y: MOD_BASE.tran.y * this.bellCurveX(-absDistance.x.c, absDistance.y * .65),
          }
        };
        break;
      case 2:
        return {
          tran: {
            x: MOD_BASE.tran.x * absDistance.x.c,
            y: MOD_BASE.tran.y * this.logisticsCurveX(-absDistance.x.c, -absDistance.y),
          }
        };
        break;
    }
  }

  static getDynamicMouseModelMovement(modelIndex, absDistance, mod) {
    switch (modelIndex) {
      case 0:
        return {
          tran: {
            z: mod.lr.tran.z * this.bellXYPlusLogisticsXWithoutY(absDistance.x.c, absDistance.x.l, absDistance.y)
          },
          rot: {
            x: mod.lr.rot.x * this.logisticsCurveX(absDistance.x.c, -absDistance.y),
            y: mod.lr.rot.y * absDistance.x.l
          }
        }
        break;
      case 1:
        return {
          tran: {
            z: mod.c.tran.z * this.bellCurveXY(absDistance.x.c, absDistance.y)
          },
          rot: {
            x: mod.c.rot.x * this.bellCurveX(-absDistance.x.c, absDistance.y),
            y: mod.c.rot.y * absDistance.x.c
          }
        };
        break;
      case 2:
        return {
          tran: {
            z: mod.lr.tran.z * this.bellXYPlusLogisticsXWithoutY(-absDistance.x.c, absDistance.x.r, absDistance.y)
          },
          rot: {
            x: mod.lr.rot.x * this.logisticsCurveX(-absDistance.x.c, -absDistance.y),
            y: mod.lr.rot.y * absDistance.x.r
          }
        };
        break;
    }
  }

  // --- Math functions ---
  // NOTE The LaTeX equations are the experimental ones for localDistance
  // TODO Find out what exactly I meant by this ^
  static bellCurveX(x, y) {
    // Bell curve
    // \left(-0.4\cdot e^{-1.25\cdot x^2}-0.5\right)\cdot y
    return ((0.45 * Math.pow(Math.E, -1.25 * Math.pow(x, 2))) + 0.45) * y;
  }
  static logisticsCurveX(x, y) {
    const yDistance = 1.5;
    // Logistics curve
    // \left(\frac{1}{1+e^{-2.4x}}-1\right)\cdot y
    return ((1 / (yDistance + Math.pow(Math.E, -2.4 * x))) - 1/yDistance) * y;
  }
  static bellCurveXY(x, y) {
    // 3 dimensional bell function
    // -2\ \cdot \ e^{-.4\cdot x^2}\cdot e^{-1.25\cdot y^2}
    return -.2 *
      Math.pow(Math.E, -0.4 * Math.pow(x, 2)) *
      Math.pow(Math.E, -1.25 * Math.pow(y, 2));
  }
  static bellXYPlusLogisticsXWithoutY(xCenter, x, y) {
    // I'm sorry I don't have a better name for this
    return .9 * this.logisticsCurveX(xCenter, 1) + this.bellCurveXY(x, y);
  }
}
