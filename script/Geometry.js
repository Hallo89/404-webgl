'use strict';
import { GeometryAnimator } from './GeometryAnimator.js';
import { ColorInformation } from './ColorInformation.js';
import { GeometryParser } from './GeometryParser.js';
import * as webgl from './gl.js';

const gl = webgl.gl;

export class Geometry extends GeometryAnimator {
  colorBuffer = gl.createBuffer();
  pointsBuffer = gl.createBuffer();

  points;
  color;

  constructor(objFormat, index) {
    super(index);

    const parserResult = GeometryParser.parseGeometry(objFormat);
    this.points = parserResult.points;
    this.color = new ColorInformation(this.points, parserResult.colorStencil, 3);
  }

  /** Set the model's x translation and x, y, z scale.  */
  setPositionAndScale(scaleFactor) {
    this.stateHandler.states.base.assignNewTransform({
      tran: {
        x: (this.index - 1) * 10 * scaleFactor, // (-10, 0, 10)
      },
      scale: {
        x: scaleFactor,
        y: scaleFactor,
        z: scaleFactor
      }
    });
  }

  // ---- Colors ----
  setColorDefault() {
    return this.color.setDefault();
  }
  setColorByDistance(localDistance, startingHue) {
    return this.color.setByDistance(this.points, this.index, localDistance, startingHue);
  }

  // ---- WebGL ----
  draw() {
    GLBoiler.setMatrix(gl, 'scale', webgl.mScale, Object.values(this.stateHandler.state.scale));
    GLBoiler.setMatrix(gl, 'translate', webgl.mTranslate, Object.values(this.stateHandler.state.tran));
    GLBoiler.setMatrix(gl, 'rotateX', webgl.mRotateX, [this.stateHandler.state.rot.x]);
    GLBoiler.setMatrix(gl, 'rotateY', webgl.mRotateY, [this.stateHandler.state.rot.y]);
    // this.setMatrix('rotateZ', webgl.mRotateZ, [this.controls.state.rot.z]);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.color.data, gl.STATIC_DRAW);

    gl.vertexAttribPointer(webgl.aColor, 3, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.pointsBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.points, gl.STATIC_DRAW);

    gl.vertexAttribPointer(webgl.aPoints, 3, gl.FLOAT, false, 0, 0);

    // Enabling the Vertex Array Object will make it consume the buffers
    // Otherwise gl.vertexAttrib[3][f][v] will write to the attribute
    gl.enableVertexAttribArray(webgl.aPoints);
    gl.enableVertexAttribArray(webgl.aColor);

    gl.drawArrays(gl.TRIANGLES, 0, this.points.length / 3);

    gl.disableVertexAttribArray(webgl.aPoints);
    gl.disableVertexAttribArray(webgl.aColor);
  }
}
