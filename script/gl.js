'use strict';
import { DIMENSIONS } from './GeometryParser.js';

const vertex = `#version 300 es

in vec4 points;
in vec3 color;

uniform mat4 perspective;
uniform mat4 translate;
uniform mat4 rotateX;
uniform mat4 rotateY;
uniform mat4 origin;
uniform mat4 scale;

out vec3 fragColor;

void main() {
  fragColor = color;
  mat4 matrix = perspective * translate * rotateX * rotateY * scale * origin;
  gl_Position = matrix * points;
}
`;

const fragment = `#version 300 es

precision mediump float;

in vec3 fragColor;

out vec4 fragOut;

void main() {
  fragOut = vec4(fragColor, 1);
}
`;

export const canvas = GLBoiler.getCanvasByTag();
/** @type WebGL2RenderingContext */
export const gl = GLBoiler.getContext(canvas);

GLBoiler.setDimensions(gl, canvas);

const program = GLBoiler.createProgram(gl, vertex, fragment);

export const aColor = gl.getAttribLocation(program, 'color');
export const aPoints = gl.getAttribLocation(program, 'points');
export const mPerspective = gl.getUniformLocation(program, 'perspective');
export const mTranslate = gl.getUniformLocation(program, 'translate');
export const mOrigin = gl.getUniformLocation(program, 'origin');
export const mRotateX = gl.getUniformLocation(program, 'rotateX');
export const mRotateY = gl.getUniformLocation(program, 'rotateY');
export const mScale = gl.getUniformLocation(program, 'scale');

gl.useProgram(program);

// model dimensions in OpenGL 3D: [x] 7 * [y] 12 * [z] 3
GLBoiler.setMatrix(gl, 'origin', mOrigin, [
  DIMENSIONS.x * -0.5,
  DIMENSIONS.y * -0.5,
  DIMENSIONS.z * -0.5
]);
setPerspective();

GLBoiler.enable(gl, gl.CULL_FACE, gl.DEPTH_TEST);


export function setPerspective() {
  GLBoiler.setMatrix(gl, 'perspective', mPerspective, [canvas, 1.75, 1, 150]);
}
