'use strict';
import { Geometry } from './Geometry.js';
import { GeometryAnimator } from './GeometryAnimator.js';
import { canvas, gl, setPerspective } from './gl.js';
import { modelFormats } from './modelFormats.js';

export class Controller {
  keyPressIsAnimating = new WeakSet();
  keyPressCounter = 0;
  proximityHue = 0;

  mouseIsDown = false;

  scaleFactor;
  mousePos = {
    x: 0,
    y: 0
  };
  modelDims = {
    x: 0,
    y: 0
  };
  models;

  constructor() {
    this.redrawAll = this.redrawAll.bind(this);
    this.resizeCanvas = this.resizeCanvas.bind(this);
    this.redrawBounce = this.redrawBounce.bind(this);
    this.redrawAllClickIn = this.redrawAllClickIn.bind(this);
    this.redrawAllClickOut = this.redrawAllClickOut.bind(this);
    this.mouseDown = this.mouseDown.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this.mouseMove = this.mouseMove.bind(this);
    this.keyDown = this.keyDown.bind(this);
    this.keyUp = this.keyUp.bind(this);

    this.models = [
      new Geometry(modelFormats[4], 0),
      new Geometry(modelFormats[0], 1),
      new Geometry(modelFormats[4], 2),
    ];

    canvas.addEventListener('contextmenu', e => {
      e.preventDefault();
    });
    window.addEventListener('resize', this.resizeCanvas);
    window.addEventListener('pointerdown', this.mouseDown);
    window.addEventListener('pointerup', this.mouseUp);
    window.addEventListener('pointermove', this.mouseMove);
    window.addEventListener('keydown', this.keyDown);
    window.addEventListener('keyup', this.keyUp);

    this.updateScaleFactor();
    this.redrawAll();
  }

  // ---- Events ----
  // -> Mouse events
  async mouseDown(e) {
    const absDistance = this.getMouseAbsDistance(e.clientX, e.clientY);
    this.mousePos.x = e.clientX;
    this.mousePos.y = e.clientY;

    // `redrawAllClickIn` also continuously sets `mouseIsDown` to true
    await this.animateAll('animateClickIn', this.redrawAllClickIn, absDistance);
  }

  async mouseUp(e) {
    this.mouseIsDown = false;
    await this.animateAll('animateClickOut', this.redrawAllClickOut);
    // This is necessary because `mouseIsDown` is set in `redrawAllClickIn`
    this.mouseIsDown = false;
  }

  mouseMove(e) {
    const absDistance = this.getMouseAbsDistance(e.clientX, e.clientY);
    const localDistance = this.getMouseLocalDistance(e.clientX, e.clientY);

    this.mousePos.x = e.clientX;
    this.mousePos.y = e.clientY;

    this.models.forEach(model => {
      model.setFlatMouseMove(absDistance);
      model.setDynamicMouseMove(absDistance);
    })

    if (this.mouseIsDown) {
      this.models.forEach(model => {
        model.setColorByDistance(localDistance, this.proximityHue);
        model.setMouseClick(absDistance);
      });

      this.incrementHue();
    }

    this.redrawAll();
  }

  // -> Key events
  keyUp(e) {
    const pressedNum = parseInt(e.key);
    if (!Number.isNaN(pressedNum)) {
      this.keyPressCounter = ++this.keyPressCounter % 3;
    }
  }
  async keyDown(e) {
    const pressedNum = parseInt(e.key);
    if (!Number.isNaN(pressedNum)) {
      const currentPos = this.keyPressCounter;

      const oldModel = this.models[currentPos];
      const newModel = this.getNewModel(pressedNum, currentPos);

      if (!this.keyPressIsAnimating.has(oldModel)) {
        await oldModel.animateLetterOut(this.redrawAll);
      } else {
        oldModel.stateHandler.resetState('animBounce');
      }

      newModel.stateHandler.importAllStatesFrom(oldModel.stateHandler);
      this.models[currentPos] = newModel;
      this.keyPressIsAnimating.add(newModel);
      // this.keyPressAnimatingIndex = currentPos;

      newModel.animateLetterIn();
      await this.animateModelBounceWave(currentPos, this.redrawBounce);

      this.keyPressIsAnimating.delete(newModel);
    }
  }

  // ---- Draw functions ----
  /**
   * @param { number } modelIndex
   * @param { import('@lib/controls3d/script/State3D.js').Animation.AnimationData }
   */
  redrawBounce(modelIndex, { animation }) {
    const model = this.models[modelIndex];
    const intro = animation.duration / 6;
    const outro = animation.duration - (animation.duration / 6);
    const localDistance = this.getMouseLocalDistance(
      (this.modelDims.x / 2) + this.modelDims.x * modelIndex,
      (this.modelDims.y / 2)
    );

    this.incrementHue();
    // Set interpolation baseline
    model.setColorByDistance(localDistance, this.proximityHue);
    if (animation.elapsed < intro) {
      const progress = 1 - (animation.elapsed / intro);
      model.color.interpolateFromSnapshotToDefault(progress);
    } else if (animation.elapsed > outro) {
      const progress = 1 - (animation.elapsed - animation.duration) / (outro - animation.duration);
      model.color.interpolateFromSnapshotToDefault(progress);
    }
    this.redrawAll();
  }

  /**
   * @param { import('@lib/controls3d/script/State3D.js').Animation.AnimationData }
   */
  redrawAllClickIn({ animation }) {
    this.mouseIsDown = true;
    this.interpolateModelColorsFromMousePos(1 - (animation.elapsed / animation.duration));
    this.redrawAll();
  }
  /**
   * @param { import('@lib/controls3d/script/State3D.js').Animation.AnimationData }
   */
  redrawAllClickOut({ animation }) {
    this.interpolateModelColorsFromMousePos(animation.elapsed / animation.duration);
    this.redrawAll();
  }

  // ---- Misc helpers ----
  incrementHue() {
    this.proximityHue = ++this.proximityHue % 360;
  }

  interpolateModelColorsFromMousePos(progress) {
    const localDistance = this.getMouseLocalDistance(this.mousePos.x, this.mousePos.y);
    this.models.forEach(model => {
      model.setColorByDistance(localDistance, this.proximityHue);
      model.color.interpolateFromSnapshotToDefault(progress);
    });
  }

  // ---- Model helpers ----
  /** @param { number } modelIndex */
  async animateModelBounceWave(modelIndex, drawFn, direction = 0) {
    modelIndex = modelIndex + direction;
    if (modelIndex < 0 || modelIndex > this.models.length - 1) return;

    const promises = [
      this.models[modelIndex].animateLetterBounce(drawFn.bind(this, modelIndex)),
      await GeometryAnimator.sleep(105),
    ];
    if (direction <= 0) {
      promises.push(this.animateModelBounceWave(modelIndex, drawFn, -1));
    }
    if (direction >= 0) {
      promises.push(this.animateModelBounceWave(modelIndex, drawFn,  1));
    }
    return Promise.all(promises);
  }

  /**
   * Calculate the scale factor of one model by the current window dimensions
   * and update the (approximated) {@link modelDims} and the model's transformation.
   */
  updateScaleFactor() {
    this.scaleFactor = Math.min(1, (window.innerWidth * 0.78) / window.innerHeight);

    this.modelDims.x = 390 * this.scaleFactor;
    this.modelDims.y = 760 * this.scaleFactor;

    for (const model of this.models) {
      model.setPositionAndScale(this.scaleFactor);
    }
  }

  getNewModel(modelNumber, index) {
    const newModel = new Geometry(modelFormats[modelNumber], index);
    newModel.setPositionAndScale(this.scaleFactor);
    return newModel;
  }

  /**
   * Execute function `animationFunctionName` on all models
   * with the supplied arguments.
   * Returns the the last called function to enable the animation to be awaited.
   */
  animateAll(animationFnName, callback, ...args) {
    for (let i = 0; i < this.models.length; i++) {
      const model = this.models[i];
      if (i === this.models.length - 1) {
        return model[animationFnName](...args, callback);
      } else {
        model[animationFnName](...args);
      }
    }
  }

  // ---- Computation helpers ----
  /**
   * Get the distance of the mouse to the models' centers.
   * y is the same for all models,
   * x is separated by the three models on the left (l), center (c) and right (r).
   */
  getMouseAbsDistance(x, y) {
    return {
      x: {
        // left, center, right focused
        l: (x - (window.innerWidth / 2 - this.modelDims.x)) / (this.modelDims.x / 2),
        c: (x - window.innerWidth / 2) / (this.modelDims.x * 3 / 2),
        r: (x - (window.innerWidth / 2 + this.modelDims.x)) / (this.modelDims.x / 2)
      },
      y: (y - window.innerHeight / 2) / (this.modelDims.y / 2),
    };
  }
  /**
   * Get the distance of the mouse to the models' local space,
   * where every model has an (approximated) [0, 1] bounding box
   * with (0 | 0) on the bottom left and (1 | 1) on the top right.
   * y is the same for all models,
   * x is separated by the three models.
   */
  getMouseLocalDistance(x, y) {
    // TODO: apply this for this.modelDims?
    const realDimY = this.modelDims.y * 0.75;
    return {
      x: {
        0: ((x - ((window.innerWidth - (this.modelDims.x * 3)) / 2) - (this.modelDims.x * 0)) / this.modelDims.x),
        1: ((x - ((window.innerWidth - (this.modelDims.x * 3)) / 2) - (this.modelDims.x * 1)) / this.modelDims.x),
        2: ((x - ((window.innerWidth - (this.modelDims.x * 3)) / 2) - (this.modelDims.x * 2)) / this.modelDims.x)
      },
      y: ((window.innerHeight - y - ((window.innerHeight - realDimY) / 2)) / realDimY)
    };
  }


  // ---- WebGL helpers ----
  redrawAll() {
    GLBoiler.clearAll(gl);
    this.models.forEach(model => {
      model.draw();
    });
  }

  resizeCanvas() {
    GLBoiler.setDimensions(gl, canvas);
    setPerspective();
    this.updateScaleFactor();
    this.redrawAll();
  }
}
