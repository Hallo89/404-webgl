import { DIMENSIONS } from './GeometryParser.js';
const { x, y, z } = DIMENSIONS;

export const modelFormats = {
  0: [{
    vertices: [
      [2, z],
      [0, 2],
      [z, 2],
      [2, 0],
      [4, 2],
      [5, 0],
      [5, z],
      [x, 2],
      [5, 9],
      [x, 10],
      [4, 10],
      [5, y],
      [z, 10],
      [2, y],
      [2, 9],
      [0, 10],
      [2, z],
      [0, 2]
    ]
  }],

  1: [
    {
      name: "foot",
      vertices: [
        [0, 2],
        [0, 0],
        [2.5, 2], // Gradient smoothness vertex
        [2.5, 0], // ^
        [4.5, 2], // ^
        [4.5, 0], // ^
        [x, 2],
        [x, 0]
      ]
    },
    {
      // TODO perhaps: cullfacing
      name: "body",
      vertices: [
        [4.5, y],
        [2.5, y],
        [4.5, 2],
        [2.5, 2]
      ],
    },
    {
      name: "tip",
      vertices: [
        [0, 9.5],
        [0, x],
        [2.5, y],
        [2.5, 9.5]
      ],
    }
  ],

  2: [
    {
      name: "foot",
      vertices: [
        [0, 2],
        [0, 0],
        [3.5, 2], // Gradient matching vertex
        [3.5, 0], // ^
        [x, 2],
        [x, 0]
      ]
    },
    {
      name: "base",
      vertices: [
        [1, 2],
        [3.5, 2],
        [5, 6],
        [x, 5.5],
        [5, 9],
        [x, 10],
        [4, 10],
        [5, y],
        [2.5, 10],
        [2, y],
        [0, 7.5],
        [0, 10]
      ]
    }
  ],

  3: [
    {
      name: "lower base",
      vertices: [
        [0, z],
        [0, 1],
        [1, 2],
        [1, 0],
        [4, 2],
        [5, 0],
        [5, z],
        [x, 2],
        [5, 4],
        [x, 5],
        [4, 5],
        [6, 6]
      ]
    },
    {
      name: "upper base",
      //This is clonable from lower base with the pattern:
      //>> [n - 1, n, n - z, n - 2, n - 5, n - 4, ...] | with n = vertices[i]
      // clone: {
      //   source: "lower base",
      //   axis: "y",
      //   origin: "6"
      // }
      vertices: [
        [4, x],
        [6, 6],
        [5, 8],
        [x, x],
        [5, 9],
        [x, 10],
        [4, 10],
        [5, y],
        [1, 10],
        [1, y],
        [0, 9],
        [0, 11]
      ]
    },
    {
      name: "center supplement",
      vertices: [
        [2, x],
        [2, 5],
        [4, x],
        [4, 5],
        [6, 6]
      ]
    }
  ],

  4: [
    {
      name: "L base",
      vertices: [
        [2, y],
        [0, y],
        [2, 4.5],
        [0, 3.5],
        [2, 4.5], //Duplicate vertex, parser restriction
        [1, 2.5],
        [3.5, 4.5], // Unnecessary vertex for gradient matching with top & bottom bases
        [3.5, 2.5], // ^
        [5.5, 4.5], // ^
        [5.5, 2.5], // ^
        [x, 4.5],
        [x, 2.5]
      ]
    },
    {
      name: "top base",
      vertices: [
        [5.5, 8],
        [3.5, 8],
        [5.5, 4.5],
        [3.5, 4.5]
      ]
    },
    {
      name: "bottom base",
      vertices: [
        [5.5, 2.5],
        [3.5, 2.5],
        [5.5, 0],
        [3.5, 0]
      ]
    }
  ],

  5: [{
    vertices: [
      [0, z],
      [0, 1],
      [1, 2],
      [1, 0],
      [4, 2],
      [5, 0],
      [5, z],
      [x, 2],
      [5, 4],
      [x, 5],
      [4, 5],
      [5, x],
      [0, 5],
      [2, x],
      [0, y],
      [2, 10],
      [6, y],
      [6, 10],
      [x, 11],
      [x, 9]
    ]
  }],

  6: [{
    vertices: [
      [2, x],
      [2, 4],
      [z, x],
      [z, 5],
      [5, x],
      [4, 5],
      [x, 5],
      [5, 4],
      [x, 2],
      [5, z],
      [5, 0],
      [4, 2],
      [2, 0],
      [z, 2],
      [0, 2],
      [2, z],
      [0, 10],
      [2, 9],
      [2, y],
      [z, 10],
      [6, y],
      [6, 10],
      [x, 11],
      [x, 9]
    ]
  }],

  7: [
    {
      name: "stem",
      vertices: [
        [0, 0],
        [2, 0],
        [0, 5.5],
        [2, 5],
        [4.5, 10],
        [x, 10]
      ]
    },
    {
      name: "top part",
      vertices: [
        [x, 10],
        [x, y],
        [4.5, 10], // Gradient smoothness vertex
        [4.5, y], // ^
        [0, 10],
        [0, y]
      ]
    }
  ],

  8: [{
    vertices: [
      [6, 6],
      [4, 5],
      [x, 5],
      [5, 4],
      [x, 2],
      [5, z],
      [5, 0],
      [4, 2],
      [2, 0],
      [z, 2],
      [0, 2],
      [2, z],
      [0, 5],
      [2, 4],
      [1, 6],
      [z, 5],
      [z, x],
      [4, 5],
      [4, x],
      [6, 6],
      [5, 8],
      [x, x],
      [5, 9],
      [x, 10],
      [4, 10],
      [5, y],
      [z, 10],
      [2, y],
      [2, 9],
      [0, 10],
      [2, 8],
      [0, x],
      [z, x],
      [1, 6]
    ]
  }],

  9: [{
    vertices: [
      [0, z],
      [0, 1],
      [1, 2],
      [1, 0],
      [4, 2],
      [5, 0],
      [5, z],
      [x, 2],
      [5, 9],
      [x, 10],
      [4, 10],
      [5, y],
      [z, 10],
      [2, y],
      [2, 9],
      [0, 10],
      [2, 8],
      [0, x],
      [z, x],
      [2, 5],
      [4, x],
      [5, 5],
      [5, 8]
    ]
  }]
};
